/**
 * Big Ten Inch Record.
 * get RGB color code from args and draw Archimedes's spiral with these.
 * 
 * @author @deconbatch
 * @version 0.1
 * Processing 3.2.1
 * created : 19.11.03
 */

void setup() {
  size(980, 980);
  rectMode(CENTER);
  smooth();
  noStroke();
  noLoop();
}

void draw() {

  int   frameMax  = 3; // draw 3 images
  int   pointMax  = 5000;
  float spiralGap = 2.0;

  FetchColor fc = new FetchColor();

  // I'll use 5 colors as 2 background and 3 foreground.
  color[][] bgColor = new color[3][2];
  color[][] fgColor = new color[3][3];
  for (int i = 0; i < 2; i++) {
    bgColor[0][i] = fc.getColor(i);
  }
  for (int i = 0; i < 3; i++) {
    fgColor[0][i] = fc.getColor(i + 2);
  }
  
  // make 2 addtion color BGR, BRG
  for (int i = 0; i < 2; i++) {
    bgColor[1][i] = color(blue(bgColor[0][i]), green(bgColor[0][i]), red(bgColor[0][i]));
    bgColor[2][i] = color(green(bgColor[0][i]), blue(bgColor[0][i]), red(bgColor[0][i]));
  }
  for (int i = 0; i < 3; i++) {
    fgColor[1][i] = color(blue(fgColor[0][i]), green(fgColor[0][i]), red(fgColor[0][i]));
    fgColor[2][i] = color(green(fgColor[0][i]), blue(fgColor[0][i]), red(fgColor[0][i]));
  }

  colorMode(RGB, 255, 255, 255, 100);
  blendMode(BLEND);
  translate(width / 2, height / 2);

  for (int frameCnt = 0; frameCnt < frameMax; ++frameCnt) {

    float startFgc = random(1.0);
    float startAlp = random(1.0);
    float startSiz = random(1.0);

    int bgIndex = frameCnt % 2;
    int fgIndex = frameCnt % 3;

    background(
               red(bgColor[frameCnt][bgIndex]),
               green(bgColor[frameCnt][bgIndex]),
               blue(bgColor[frameCnt][bgIndex]),
               100.0
               );
    noStroke();
    rotate(HALF_PI);
    
    for (int pointCnt = 0; pointCnt < pointMax; ++pointCnt) {

      float pointRatio = map(pointCnt, 0, pointMax, 0.0, 1.0);
      float divRotate = sqrt(pointCnt) * 2.0;
      float radian = TWO_PI / divRotate  * pointCnt;
      float pX = spiralGap * radian * cos(radian);
      float pY = spiralGap * radian * sin(radian);

      int   fgcIndex  = (fgIndex + floor(map(sin(TWO_PI * (startFgc + pointRatio * spiralGap * 20.0)), -1.0, 1.0, 0.0, 2.9))) % 3;
      float pointAlp  = map(sin(TWO_PI * (startAlp + pointRatio * spiralGap * 40.0)), -1.0, 1.0, 30.0, 100.0);
      float pointSiz  = map(sin(TWO_PI * (startSiz + pointRatio * spiralGap * 15.0)), -1.0, 1.0, 0.5, 1.0) * spiralGap * 4.5;
      fill(
           red(fgColor[frameCnt][fgcIndex]),
           green(fgColor[frameCnt][fgcIndex]),
           blue(fgColor[frameCnt][fgcIndex]),
           pointAlp
           );
      ellipse(pX, pY, pointSiz, pointSiz);

    }

    casing();
    saveFrame("frames/" + String.format("%04d", frameCnt+1) + ".png");

  }
  exit();
}

/**
 * casing : draw fancy casing
 */
private void casing() {
  fill(0.0, 0.0, 0.0, 0.0);
  strokeWeight(54.0);
  stroke(0.0, 0.0, 0.0, 100.0);
  rect(0.0, 0.0, width, height);
  strokeWeight(50.0);
  stroke(255.0, 255.0, 255.0, 100.0);
  rect(0.0, 0.0, width, height);
  noStroke();
  noFill();
}

/**
 * FetchColor : get 5 RGB color infomation from args and sort descending them by brightness.
 * 
 */
import java.util.Arrays;
import java.util.Comparator;

class FetchColor {

  int colorCnt = 5;
  color[] colorAry = new color[colorCnt];
  
  FetchColor() {
    colorMode(RGB, 255);
    for (int i = 0; i < colorCnt; i++) {
      colorAry[i] = color(255, 255, 255);
    }
  
    // get 5 RGB color infomation from args.
    // and translate RGB to HSB
    color[] argColor = new color[colorCnt];
    float[] baseHue  = new float[colorCnt];
    float[] baseSat  = new float[colorCnt];
    float[] baseBri  = new float[colorCnt];
    if (args == null) {
      for (int i = 0; i < colorCnt; i++) {
        colorMode(HSB, 360, 100, 100, 100);
        baseHue[i]  = random(360.0);
        baseSat[i] = 40.0;
        baseBri[i] = 10.0 + i * 10.0;
        argColor[i] = color(baseHue[i], baseSat[i], baseBri[i], 100.0);
      }
    } else {
      int cnt = 0;
      int i = 0;
      float r = 0.0;
      float g = 0.0;
      float b = 0.0;
      for (String s : args[0].split(",", 0)) {
        if (cnt % 3 == 0) r = float(s);
        if (cnt % 3 == 1) g = float(s);
        if (cnt % 3 == 2) {
          b = float(s);
          colorMode(RGB, 255);
          argColor[i] = color(r, g, b);
          colorMode(HSB, 360, 100, 100, 100);
          baseHue[i] = hue(argColor[i]);
          baseSat[i] = saturation(argColor[i]);
          baseBri[i] = brightness(argColor[i]);
          i++;
        }
        cnt++;
      }
    }

    // sort descending by brightness
    colorMode(RGB, 255);
    float[][] sortBri = new float[colorCnt][2];
    for (int i = 0; i < colorCnt; i++) {
      sortBri[i][0] = baseBri[i];
      sortBri[i][1] = i;
    }
    Arrays.sort(sortBri, new Comparator<float[]>() {
        @Override
        public int compare(float[] a, float[] b) {
          if(a[0] > b[0]) {
            return 1;
          } else if(a[0] < b[0]) {
            return -1;
          }
          return 0;
        }
      }
      );
    for (int i = 0; i < colorCnt; i++) {
      colorAry[i] = argColor[int(sortBri[i][1])];
    }
  }  

  color getColor(int i) {
    return colorAry[i];
  }

}
